import ReactDOM from 'react-dom';
import React from 'react';
import './index.css';
import Setup from './pages/Setup';
import Error from './pages/Error'
import * as serviceWorker from './serviceWorker';
import Messaging from './pages/Messaging';

try {
  const location = window.location.href.split('/');

  switch (location[location.length -1]){
  case 'messaging':
      ReactDOM.render(<Messaging />, document.getElementById('root'));
    break;
  case '':
      ReactDOM.render(<Setup />, document.getElementById('root'));
    break;
  case 'error500':
        ReactDOM.render(<Error code={500} />, document.getElementById('root'));
      break;
  case 'error':
      ReactDOM.render(<Error />, document.getElementById('root'));
    break;
  default:
      ReactDOM.render(<Error code={404} />, document.getElementById('root'));
    break;
  }
}
catch (Error){
  ReactDOM.render(<Error code={500} />, document.getElementById('root'));
}

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();