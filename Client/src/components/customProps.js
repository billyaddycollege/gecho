export const sentOrRecievedEnum = {
    sent : "sent",
    recieved: "received"
}

export const isSentOrRecieved = (props, propName, componentName) => {
    const prop = props[propName];
    let valid = false;
    Object.values(sentOrRecievedEnum).forEach(key => { 
        if (prop === key){
            valid = true;
        }
    });
    if (!valid){
        return new Error(`Invalid prop ${propName} passed to ${componentName}. Expected "sent" or "recieved".`);
    }
}