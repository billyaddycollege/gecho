import React from 'react';
import PropTypes from 'prop-types';

const MenuInput = ({onClick, buttonText, placeholder})  =>{

    function keydown(e) {
        var keycode = (e.keyCode ? e.keyCode : e.which);
        if(keycode === 13)
        {
            e.target.nextSibling.click();
        }
    };
    
    function mousedown(e){
        e.target.classList.add('pressed');
    };

    function mouseup(e){
        e.target.classList.remove('pressed');
    };

    return(
        <React.Fragment>
            <input 
                className="textInput" 
                type="text" 
                onKeyDown={e => keydown(e)} 
                placeholder={placeholder}
            />				
            <input 
                onMouseDown={e => mousedown(e)} 
                onMouseUp={e => mouseup(e)} 
                onClick={onClick} 
                type="button" 
                value={buttonText} 
            />
        </React.Fragment>
    )
}

MenuInput.propTypes = {
    onClick: PropTypes.func,
    placeholder: PropTypes.string,
    buttonText: PropTypes.string
}

export default MenuInput;