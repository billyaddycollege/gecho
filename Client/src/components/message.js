import React from 'react'
import PropTypes from 'prop-types'
import { isSentOrRecieved } from './customProps';

const Message = ({sentOrRecieved, value}) =>{
    console.log(sentOrRecieved)

    return (
        <div className={sentOrRecieved}>
            {value}
        </div>
    )
}

Message.propTypes = {
    sentOrRecieved: isSentOrRecieved,
    value: PropTypes.string.isRequired
}

export default Message;