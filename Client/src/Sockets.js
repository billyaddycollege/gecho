import {sentOrRecievedEnum as sentrecived } from './components/customProps';
import {addMessage} from './pages/Messaging';

var connection; 

try {
  connection = new WebSocket('ws://167.71.140.243:1337');
}
catch (e){
  alert('the server is currently unavailable, please try again later');
  window.location.assign('error500');
}
finally{
  connection = new WebSocket('ws://167.71.140.243:1337');
}

var name;
var id;

export function setupSocket () {

  if (!window.WebSocket) {
    alert('Sorry, but your browser doesn\'t support WebSocket.');
    return;
  }
  
  // if user is running mozilla then use it's built-in WebSocket
  window.WebSocket = window.WebSocket || window.MozWebSocket;

  connection.onerror = function (error) {
    alert('the server is currently unavailable, please try again later');
    window.location.assign('error500');
  };

  connection.onmessage = function (message) {
    // try to decode json (I assume that each message from server is json)
    var json;
    try {
      json = JSON.parse(message.data);
    } catch (e) {
      console.log('This doesn\'t look like a valid JSON: ',
          message.data);
      return;
    }
    
    // handle incoming message
    switch (json.type){
      case "id":
          id = json.id;
          window.sessionStorage.setItem('MyId',json.id);
        break;
      case "bad-user":
        alert('username already in use!');
        window.location.reload();
        break;
      case "connection":
          if (json.success){
            console.log('Connection established');
            window.sessionStorage.setItem('connected', true);
            console.log(window.sessionStorage.getItem('myName'))
            if (json.partnerName !== window.sessionStorage.getItem('myName')){
              window.sessionStorage.setItem('partnerName', json.partnerName);
            }
            window.location.href = 'messaging'
          }else{
            alert('partner doesnt exist')
        }
        break;
      case "error":
          alert(json.message);
          console.log(json.message);
          break;
      default:
        console.log('Unexpected json type ', json)
        break;
    }
  };
};

export function setName(userName){
  name = userName
  connection.send(JSON.stringify({
      "type":"userName",
      "name":userName
    }));
};

export function setPartner(partner){
  console.log('partner',partner);
  connection.send(JSON.stringify({
    "type":"connection",
    "partner": partner,
    "id": id
  }))
}

export function reConnect(){
  id = window.sessionStorage.getItem('MyId');
  name = window.sessionStorage.getItem('myName');
  const partnerName = window.sessionStorage.getItem('partnerName');

  const body = {
    type: "reconnect",
    id: id,
    partner: partnerName
  }

  connection.onopen = function () {
    console.log('connection open')
    connection.send(JSON.stringify(body));
  };

  connection.onmessage = function (message) {
    // try to decode json (I assume that each message from server is json)
    var json;
    try {
      json = JSON.parse(message.data);
    } catch (e) {
      return;
    }

    // handle incoming message
    switch (json.type){
      case "message":
        addMessage(sentrecived.recieved, decodeURIComponent(json.message));
        break;
      case "error":
          alert(json.message);
          break;
      default:
        console.log('Unexpected json type ', json)
        break;
    }
  };
}

export const send = (message) => {
  const body = {
    "type":"message",
    "id":window.sessionStorage.getItem('MyId'),
    "value": encodeURIComponent(message)
  }
  connection.send(JSON.stringify(body))
}