import React from 'react';

const errorPage = ({code}) =>{

    var message;

    switch (code){
        case 404:
            message = "Hey, you shouldnt be here. Go back to where you came from.";
            break;
        case 500: 
            message = "Well, this is embarassing. Something has gone terribly wrong on our end, try again later."
            break;
        default:
            message = "I honestly have no idea why you're here, something has gone very wrong."
            break;
    }

    return(
        <div className="ErrorPage">
            <h1>Gecho</h1>
            <h2>Error {code}</h2>
            <p>{message}</p>
            <input type="button" defaultValue="Let's get you home." onClick={()=>{window.location.href = '/'}}/>
        </div>
    )
}

export default errorPage