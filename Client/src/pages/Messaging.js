import React, {useState, useEffect} from 'react';
import Message from '../components/message';
import ReactDom from 'react-dom'
import PropTypes from 'prop-types'
import {sentOrRecievedEnum as sentrecived } from '../components/customProps';
import {send, reConnect} from '../Sockets';

var messages = [];

export const addMessage = (sent, value) => {
	console.log('adding')
	const newMessage = {
		"value": value,
		"sent": sent
	}
	console.log(newMessage)
	messages.push(newMessage);
	renderMessages();
}

const renderMessages = () => {
	var messageComponents = [];
	messages.forEach(message => {
		messageComponents.push(<Message sentOrRecieved={message.sent} value={message.value} />)
	})
	console.log(messageComponents);
	ReactDom.render(messageComponents, document.querySelector('.messageContainer'));
}

const Messaging = () => {
	reConnect();

	const [messages, setMessages] = useState([]);

	useEffect(() => {
		renderMessages();
	})


	function keydown(e) {
		var keycode = (e.keyCode ? e.keyCode : e.which);
		if(keycode === 13)
		{
			e.target.nextSibling.click();
		}
	};
	const sendMessage = (e) => {
		const messageValue = e.target.previousSibling.value;
		e.target.previousSibling.value = "";

		addMessage(sentrecived.sent, messageValue);

		send(messageValue)
	}

	return(
		<div className="mainContainer">			
			<div className="messageContainer">
			</div>
			<div className="controlRow">
				<div className="controls">
					<input type="text" onKeyDown={e => {keydown(e)}}/>
					<input type="button" onClick={e => {sendMessage(e)}} value="send"/>
				</div>
			</div>
		</div>
	)
}

export default Messaging