import React from 'react';
import MenuInput from '../components/menu';
import {setName, setPartner,setupSocket} from '../Sockets';

const Setup = () => {
    setupSocket();
    window.sessionStorage.clear();
    function setPartnerId(e,storageName) {
        let textInput = e.target.previousSibling;
        if (textInput.value.length > 0) {
            e.target.disabled = true;
            textInput.disabled = true;
            textInput.classList.remove('error');
            textInput.classList.add('valid');
            setPartner(textInput.value);
            window.sessionStorage.setItem(storageName,textInput.value);
        }
        else{
            textInput.classList.add('error');
            textInput.placeholder = "Enter a valid ID"
        }
    }

    function setUserName(e,storageName){
        let textInput = e.target.previousSibling;
        if (textInput.value.length > 0) {
            e.target.disabled = true;
            textInput.disabled = true;
            textInput.classList.remove('error');
            textInput.classList.add('valid');
            window.sessionStorage.setItem(storageName,textInput.value);
            setName(textInput.value);
        }
        else {
            textInput.classList.add('error');
        }
    }

    return(
        <React.Fragment>
            <h1>Gecho</h1>
            <div className="setupControlRow">
                <div className="setupControls">
                    <div>
                        <MenuInput placeholder={"Please choose a name"} onClick={e => setUserName(e,'myName')} buttonText={"Set"}/>
                    </div>
                    <div>
                        <MenuInput placeholder={"Enter the name of your partner"} onClick={e => setPartnerId(e,'partnerName')} buttonText={"Set"} disabled/>
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
}

export default Setup;