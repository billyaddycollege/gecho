var http = require('http');

webSocketsServerPort = 1337;

const WebSocketServer = require('websocket').server;

var server = http.createServer(function(request, response) {
    // process HTTP request. Since we're writing just WebSockets
    // server we don't have to implement anything.
});

server.listen(webSocketsServerPort, () => {  
    console.log((new Date()) + " Server is listening on port "+ webSocketsServerPort) 
});

// create the server
wsServer = new WebSocketServer({
    httpServer: server
});

function UUID() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}  

var users = [];

class user{
    constructor(name,id,connection) {
        this.id = id;
        this.name = name;
        this.connection = connection;
        this.connectionId = connection.id;
        this.partnerConnection = {};
    }
    setConnection(connection){
        this.connection = connection;
    }
    setName(newName) {
        console.log(newName);
        this.name = newName;
    }   
    setPartner(partnerConnection) {
        this.partnerConnection = partnerConnection.connection;
    }     
    broadcast(jsonMessage){
        this.messagePeer(jsonMessage);
        this.connection.send(JSON.stringify(jsonMessage));
    }
    messagePeer(jsonMessage) {
        this.partnerConnection.send(JSON.stringify(jsonMessage));
    }
    get jsonObj() {
        return ({
            id: this.id,
            name: this.name,
            connection: this.connection
        });
    }
    get jsonStr() {
        return (JSON.stringify({
            id: this.id,
            name: this.name,
            connection: this.connection,
        }));
    }
}

function getUserById(id){
    return( users.find(user => {
        if(user.id === id) return user} )
    );
}

function getUserByName(name){
    return( users.find(user => {
        if(user.name === name) return user} )
    );
}

function availableName(name) {
    return getUserByName(name) === undefined? true: false;
}

function diconectUser(id){
   users = users.filter(user => user.connectionId !== id)
}

// WebSocket server
wsServer.on('request', function(request) {
    var connection = request.accept(null, request.origin);

    // This is the most important callback for us, we'll handle
    // all messages from users here.
    connection.on('message', function(message) {
        if (message.type === 'utf8') {
            var json;
            try {
                json = JSON.parse(message.utf8Data);
                console.log('valid json')
            } catch (e) {
                return;
            }
            // handle incoming message
            const requestingUserId = json.id;
            let requestingUser;
            let peerUser;
            if (requestingUserId || json.type === "userName"){
                switch (json.type){
                    case "userName":
                        let available = availableName(json.name);
                        if (available){
                            let userid = UUID();
                            
                            const newUser = new user(
                                json.name,
                                userid,
                                connection
                            );
                            users.push(newUser)
                            connection.send(JSON.stringify({type:"id","id":userid}));
                        }
                        else {
                            connection.send(JSON.stringify({"type":"bad-user"}))
                            diconectUser(connection.id);
                        }
                        break;
                    case "connection":
                        console.log('request recieved')
                        requestingUser = getUserById(requestingUserId);
                        peerUser = getUserByName(json.partner)
                        
                        if (peerUser){
                            requestingUser.setPartner(peerUser);
                            peerUser.setPartner(requestingUser);
                            requestingUser.broadcast({type: "connection","success":true, "partnerName": requestingUser.name});
                        }
                        else{
                            requestingUser.connection.send(JSON.stringify({"type":"connection","success":false}));
                        }
                        break;
                    case "reconnect":
                        console.log('connection established');

                        requestingUser = getUserById(json.id);
                        requestingUser.setConnection(connection);

                        peerUser = getUserByName(json.partner)
                        peerUser.setPartner(requestingUser);
                        requestingUser.setPartner(peerUser);
                        
                        break;
                    case "message":
                        try {
                            requestingUser = getUserById(json.id);
                            const message = json.value;
                            const body = {
                                "type":"message",
                                "message": message
                            }
                            requestingUser.partnerConnection.send(JSON.stringify(body));
                        }
                        catch (e) {
                            connection.send(JSON.stringify({type: "error","message":"an unexpected error has occured"}));
                        }
                        break;
                    default:
                        break;
                }
            }
            else{
                connection.send(JSON.stringify({type: "error","message":"User id and json.type must be sent with all requests"}));
            }
        }
    });

    connection.on('close', function(connection) {
        
    });
});
